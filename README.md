This requires some older versions of some libraries to build.

I used a Debian Wheezy chroot to simplify the process and avoid overwriting
any of my newer libraries accidentally.

When it came time to run the emulator, I set LD_LIBRARY_PATH appropriately
outside of the chroot.

Dependencies (version numbers are just what I used; you may be able to get away
with other newer or older versions):

* ffmpeg 1.0.1
* wxWidgets 2.9.5
* SDL2 2.0.8

(The rest of the package dependencies should be installable from a debian
wheezy repository, if you take that route. I don't remember what the rest
were off the top of my head, but I'm sure there are OpenGL libraries in there
somewhere. The original documentation for Dolphin 3.5 would be helpful here.)

If you don't install SDL2, but use SDL1, everything should still work except
for controller rumble support, which will be absent.
